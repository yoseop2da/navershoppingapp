//
//  RemoteList.m
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "RemoteList.h"
#import "URL.h"
#import "Item.h"
#import "XMLReader.h"

@interface RemoteList()

@property (strong) URL *url;
@property (assign) NSInteger totalCount;

@end

@implementation RemoteList

- (id)initWithURL:(URL *)aURL
{
    self = [super init];
    if(self){
        _url = aURL;
        self.remoteObjects = [NSMutableArray array];
    }
    return self;
}

- (void)load:(void (^)(void))block
{
    // request 생성 - 이 안에는 전송방식, url정보 등이 들어있다.
    NSURLRequest *request = [self makeURLRequest];
    
    //위에서 생성한 request로 서버로 요청하는 부분.
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        // 서버로부터의 응답 - data에 우리가 원하는 값이 들어있다.

        // 다음 두줄의 코드는 겨로가가 올바로 왔는지 체크하는 부분이다
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        NSInteger responseStatusCode = [httpResponse statusCode];
        
        if (responseStatusCode == 200) {
            
            //서버로부터 받은 data는 xml방식이므로 xmlConvert(외부라이브러리)를 통해 원하는 결과 값을 dictionary로 만들어준다.
            NSDictionary *xmlDictionary = [self xmlConvert:data][@"rss"][@"channel"];
            
            //풀투리프레시를 구현하기위해 totalcount의 정보는 저장하고있는다.
            self.totalCount = [xmlDictionary[@"total"][@"text"] integerValue];
            
            //일반적인 데이터를 가지고, Item 객체의 배열로 변경해준다.
            NSArray *items = xmlDictionary[@"item"];
            for (NSDictionary *itemAttributes in items) {
                Item *item = [[Item alloc] initWithDictionAry:itemAttributes];
                [self.remoteObjects addObject:item];
            }
            
            //여기서 block()는 콜백을 의미한다,,, 원하는 결과를 얻었으므로, load를 부른 곳으로 돌아간다.(searchView에서 불렀음)
            block();
        }else{
            NSLog(@"Error ...... %@",connectionError);
        }
    }];
    
}

- (BOOL)canLoadMore
{
    return ( self.totalCount > self.remoteObjects.count );
}


// =============================================================

- (NSDictionary *)xmlConvert:(NSData *)aData
{
    NSString *xmlStering = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
    
    NSError *parseError = nil;
    return [XMLReader dictionaryForXMLString:xmlStering error:&parseError];
}

- (NSString *)requestURLString
{
    NSUInteger offset = [self.remoteObjects count];
    
    NSString *urlString = [self.url.baseURL stringByAppendingFormat:@"%@%@%@%@%@%@",
                           [NSString stringWithFormat:@"/search?key=%@",self.url.apiField],
                           [NSString stringWithFormat:@"&query=%@",self.url.queryField],
                           [NSString stringWithFormat:@"&display=%d",self.url.displayField],
                           [NSString stringWithFormat:@"&start=%d",offset + 1],
                           [NSString stringWithFormat:@"&target=%@",self.url.targetField],
                           [NSString stringWithFormat:@"&sort=%@",self.url.sortField]
                           ];
    NSLog(@"urlString============= %@",urlString);
    return urlString;
}

- (NSURL *)escapedURLString:(NSString *)urlString
{
    return [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}

- (NSURLRequest *)makeURLRequest{
    NSString *urlString = [self requestURLString];
    NSURL *url = [self escapedURLString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    return request;
}



@end
