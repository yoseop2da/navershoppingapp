//
//  NSData+Additional.m
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "NSData+Additional.h"

@implementation NSData (Additional)

+ (NSData *)cachedDataWithName:(NSString *)cachedName
{
    NSString *fileName = [NSString stringWithFormat:@"/churp2_%@", cachedName];
    NSString *cachePath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [cachePath stringByAppendingPathComponent:fileName];
    
    return [NSData dataWithContentsOfFile:filePath];
}

- (void)cacheWithName:(NSString *)cacheName
{
    NSString *fileName = [NSString stringWithFormat:@"/churp2_%@", cacheName];
    NSString *cachePath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [cachePath stringByAppendingPathComponent:fileName];
    BOOL writeToFile = [self writeToFile:filePath atomically:YES];
    if (!writeToFile) {
        NSLog(@"Failed to save profile image");
    }
}

@end
