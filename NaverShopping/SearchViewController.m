//
//  SearchViewController.m
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "SearchViewController.h"
#import "ProductListViewController.h"
#import "RemoteList.h"
#import "URL.h"

@interface SearchViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong) RemoteList *remoteList;

@end

@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

// 검색버튼
- (IBAction)searchButtonTouched:(id)sender {
    [self fetchShopList];
}


// 검색어를 통한 결과요청
- (void)fetchShopList
{
    self.remoteList = [[RemoteList alloc] initWithURL:[self createURL]];
    
    [self.remoteList load:^{
        
        //위의 load메소드에서 처리된 결과를 가지고 처리를 시작한다.
        ProductListViewController *productListViewController = [[ProductListViewController alloc] initWithRemoteList:self.remoteList];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:productListViewController];
        
        //모달로 화면을 띄운다.
        [self presentViewController:navController animated:YES completion:^{}];
    }];
}

- (IBAction)mainButtonTouched:(id)sender {
    
    _searchTextField.text = @"";
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


// 임시.
- (URL *)createURL
{
    
#define kAPIKey @"13bcb9af0cfe11aa12e27ef058a21cd0"
    
    //URL을 만들어준다. 기본정보 셋팅.
    URL *url = [URL new];
    
    url.baseURL = @"http://openapi.naver.com";
    url.apiField = kAPIKey;
    url.queryField = self.searchTextField.text;
    url.displayField = 10;
    url.startField = 1;
    url.targetField = @"shop";
    url.sortField = @"asc";
    /*    sim : 유사도순 (기본값) / date : 날짜순 / asc : 가격오름차순 / dsc : 가격내림차순 */   
    return url;
}


@end
