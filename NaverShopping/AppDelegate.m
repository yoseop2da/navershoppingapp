//
//  AppDelegate.m
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "AppDelegate.h"
#import "SearchViewController.h"
#import "XMLReader.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    
    // 서치뷰 생성
    SearchViewController *searchViewController = [SearchViewController new];
    
    //네비게이션컨트롤러에 서치뷰를 루트뷰로
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:searchViewController];
    
    self.window.rootViewController = navController;
    
    
    //========================================================
    //============== 데이터 요청하고 파싱하는것만 빼놓은 부분 ==========
    [self methodForParsing];
    //========================================================
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}





- (void)methodForParsing
{
    //파싱 요청할 URL String ('bag'으로 검색하기)
    NSString *urlString = @"http://openapi.naver.com/search?key=13bcb9af0cfe11aa12e27ef058a21cd0&query=bag&display=10&start=11&target=shop&sort=asc";
    
    //파싱 요청할 URL String 을 NSURL로 변환하기
    NSURL *url = [NSURL URLWithString:urlString];
    
    //위의 URL로 요청할 request 만들기
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    //    [NSMutableURLRequest requestWithURL:url];
    
    // request 방식을 get으로 설정하기
    request.HTTPMethod = @"GET";
    
    //위에서 생성한 request로 서버로 요청하는 부분.
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        // 이곳에서는 request한 이후 data,response, connectionError을 통해서 결과를 받는다.
        NSString *info = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        // 확인해보면 xml형식으로 받은 것을 알 수 있다.
        // 콘솔창을 확인해보기
        NSLog(@"1. data : %@",info);
        
        
        // 정리되어있지 않은 xml데이터를 key,value의 묶음인 dictionary로 변경해주기
        NSError *parseError = nil;
        NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:info error:&parseError];
        NSLog(@"2. dictionay data : %@",xmlDictionary);
        
        
        // dictionary값중에서 channel정보만 가져오기.
        NSLog(@"3. channel dictionay data : %@",xmlDictionary[@"rss"][@"channel"]);
    }];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
