//
//  NSData+Additional.h
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Additional)

+ (NSData *)cachedDataWithName:(NSString *)cachedName;

- (void)cacheWithName:(NSString *)cacheName;

@end
