//
//  RemoteList.h
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import <Foundation/Foundation.h>

@class URL;

@interface RemoteList : NSObject
@property (nonatomic,strong) NSMutableArray *remoteObjects;

- (id)initWithURL:(URL *)aURL;

- (void)load:(void (^)(void))block;

- (BOOL)canLoadMore;

@end
