//
//  Item.h
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property (strong) NSString *hprice;
@property (strong) NSString *image;
@property (strong) NSString *link;
@property (strong) NSString *lprice;
@property (strong) NSString *mallName;
@property (strong) NSString *productId;
@property (strong) NSString *productType;
@property (strong) NSString *title;

- (id)initWithDictionAry:(NSDictionary *)dic;

@end
