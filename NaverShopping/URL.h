//
//  URL.h
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URL : NSObject

@property (strong) NSString *baseURL;
@property (strong) NSString *apiField;
@property (strong) NSString *queryField;
@property (assign) int displayField;
@property (assign) int startField;
@property (strong) NSString *targetField;
@property (strong) NSString *sortField;

@end
