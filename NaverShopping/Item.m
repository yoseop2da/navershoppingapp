//
//  Item.m
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "Item.h"

@implementation Item

- (id)initWithDictionAry:(NSDictionary *)dic
{
    self = [super init];
    if(self){
        [self setupAttributes:dic];
    }
    return self;
}

- (void)setupAttributes:(NSDictionary *)attributes
{
    NSArray *allKeys = attributes.allKeys;
    for (NSString *key in allKeys) {
        id value = attributes[key];
            
        [self setValue:value[@"text"] forKey:key];
    }
}

@end