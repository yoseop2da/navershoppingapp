//
//  ProductListViewController.m
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import "ProductListViewController.h"
#import "XMLReader.h"
#import "Item.h"
#import "RemoteList.h"
#import "LoadMoreCell.h"
#import "NSData+Additional.h"


@interface ProductListViewController ()
@property (strong) RemoteList *remoteList;
@property (strong) NSArray *itemsArray;

@end

@implementation ProductListViewController

- (id)initWithRemoteList:(RemoteList *)aRemoteList
{
    self = [super init];
    if(self){
        _remoteList = aRemoteList;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.itemsArray = self.remoteList.remoteObjects;
    [self registerCell];
}

// 커스텀셀
- (void)registerCell
{
    UINib *loadMoreCell = [UINib nibWithNibName:NSStringFromClass([LoadMoreCell class]) bundle:nil];
    [self.tableView registerNib:loadMoreCell forCellReuseIdentifier:kLoadMoreCellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.itemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == ([tableView numberOfRowsInSection:0] -1) && [self.remoteList canLoadMore]) {
        LoadMoreCell *cell = (LoadMoreCell *)[tableView dequeueReusableCellWithIdentifier:kLoadMoreCellIdentifier];
        [cell.activityIndicator startAnimating];
        
        [self.remoteList load:^{
            
            [self.tableView reloadData];
        }];
        return cell;
    }else{
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        Item *item = self.itemsArray[indexPath.row];
        
        cell.textLabel.text = item.title;
        
        NSData *cachedData = [NSData cachedDataWithName:item.productId];
        
        if (cachedData) {
            cell.imageView.image = [UIImage imageWithData:cachedData];
        }else{
            NSURL *url = [[NSURL alloc] initWithString:item.image];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
            
            [NSURLConnection sendAsynchronousRequest:urlRequest queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                if (((NSHTTPURLResponse *)response).statusCode == 200) {
                    [data cacheWithName:item.productId];
                    UIImage *image = [UIImage imageWithData:data];
                    cell.imageView.image = image;
                }
            }];
        }
        
        return cell;
    }
}
@end
