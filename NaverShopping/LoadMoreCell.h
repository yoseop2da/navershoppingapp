//
//  LoadMoreCell.h
//  NaverShopping
//
//  Created by ParkYoseop on 2014. 6. 7..
//  Copyright (c) 2014년 yosep2da. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kLoadMoreCellIdentifier = @"LoadMoreCellIdentifier";

@interface LoadMoreCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
